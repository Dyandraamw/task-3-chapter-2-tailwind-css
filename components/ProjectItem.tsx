import React from 'react'
import placeholder from "../assets/placeholder.png"
import Image from 'next/image'

export default function ProjectItem() {
  return (
    <div className='container bg-white opacity-90 rounded-xl flex w-[1050px] m-auto p-5 my-5  text-4xl font-medium justify-around flex'>
            <Image src={placeholder} alt="placeholder" width={200}  className='ml-5 '/>
            <div className='flex flex-col  justify-center ml-5 p-2'>
                <h1 className='font-medium text-lilac text-3xl p-2'> Bloodbank</h1>
                <h1 className='text-black text-xl p-2 '> Bloodbank is a web app that can be used to assist the process of donating blood to be more effective and efficient</h1>
            </div>
    </div>
  )
}
