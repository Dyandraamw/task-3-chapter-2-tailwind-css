import React from 'react'
import profile from "../assets/profile.png"
import Image from 'next/image'

export default function Banner() {
  return (
    <div className='container flex  w-full m-auto justify-center p-5 my-5'>  
        <div className='flex flex-col justify-center'>
            <h1 className='font-medium text-lilac text-6xl p-2'> Hi! I am Dyandra</h1>
            <h1 className='text-light-lilac text-3xl p-2'> Frontend Developer</h1>
            <h1 className='text-white text-xl p-2'> Third year undergraduate student majoring in computer science <br /> aspiring to become a frontend web developer</h1>
        </div>
        <Image src={profile} alt="App Logo" width={480}  className='ml-5 '/>
    </div>
  )
}
