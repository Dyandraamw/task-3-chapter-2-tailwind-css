import React from 'react'
import webLogo from "../assets/Logo-no-text.png"
import Image from 'next/image'

export default function Navbar() {
  return (
    <nav className="bg-black  p-4 sticky top-0 drop shadow xl z-10">
        <div className='prose prose-xl mx auto flex justify-between flex-col sm:flex-row'>
            <Image src={webLogo} alt="App Logo" width={80} height={90} className='ml-5'/>
            <div className="p-2 text-white flex justify-end items-center">
                <h1 className="font-medium text-xl mr-6 hover:text-lilac">Home</h1>
                <h1 className="font-medium text-xl mr-6 hover:text-lilac">About</h1>
                <h1 className="font-medium text-xl mr-6 hover:text-lilac">Skills</h1>
                <h1 className="font-medium text-xl mr-6 hover:text-lilac">Contact</h1>
            </div>
        </div>
    </nav>
  )
}
